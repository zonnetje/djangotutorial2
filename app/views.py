"""
Definition of views.
"""

from django.shortcuts import render
from django.http import HttpRequest
from django.template import RequestContext
from datetime import datetime

from django.conf import settings

import asyncio
import aioxmpp


loop = asyncio.get_event_loop()

local_jid = aioxmpp.JID.fromstr(settings.XMPPID)
password = settings.XMPPPASS
client = aioxmpp.PresenceManagedClient(
    local_jid,
    aioxmpp.make_security_layer(password, no_verify=True)
)

def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        {
            'title':'Home Page',
            'year':datetime.now().year,
        }
    )

def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contact.html',
        {
            'title':'Contact',
            'message':'Your contact page.',
            'year':datetime.now().year,
        }
    )

def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)

    async def st():
        print("starting the business")
        async with client.connected():
            print("prep the message")
            msg = aioxmpp.stanza.Message(
		        to=aioxmpp.JID.fromstr('remco-1@dismail.de'),
		        type_=aioxmpp.MessageType.CHAT,
		    )
            print("the message")
            # [None] is for "no XML language tag
            msg.body[None] = "something interesting"
            await client.send(msg)

    #asyncio.run_coroutine_threadsafe(st(), loop)
    loop.run_until_complete(st())


    return render(
        request,
        'app/about.html',
        {
            'title':'About',
            'message':'Your application description page.',
            'year':datetime.now().year,
        }
    )
